# README #
Result of usecase: job offer (2017)
### Steps to run the app: ###

* You should have installed postgresql;
* An empty database called __energy_consumption__ with 2 users
      * user '__flyway__' with password '__flyway__' which is superuser
      * user '__energy__' with password '__energy__';
* Run command in terminal: 

```
#!java

java -jar -Dspring.profiles.active=local EnergyConsumption-1.0-SNAPSHOT.jar
```


### REST API ###
You can check REST API at http://localhost:9900/swagger-ui.html

### Postman ###
You can find postman collection for REST API calls as well as test data.