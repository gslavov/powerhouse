CREATE TABLE profile
(
  id   BIGSERIAL PRIMARY KEY NOT NULL,
  name VARCHAR(255)
);

CREATE TABLE fraction
(
  id             BIGSERIAL PRIMARY KEY NOT NULL,
  month          VARCHAR(255),
  fraction_value REAL,
  profile_id     BIGINT,
  CONSTRAINT profile_id FOREIGN KEY (profile_id) REFERENCES profile (id)
);

CREATE TABLE meter_reading
(
  id          BIGSERIAL PRIMARY KEY NOT NULL,
  month       VARCHAR(255),
  meter_value REAL,
  profile_id  BIGINT,
  device_id  BIGINT,
  CONSTRAINT profile_id FOREIGN KEY (profile_id) REFERENCES profile (id)
);
