package com.gslavov.api;

import com.gslavov.api.model.Fraction;
import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.service.FractionService;
import com.gslavov.service.ProfileService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/fraction", produces = APPLICATION_JSON_VALUE)
public class FractionController {

    @Autowired
    private FractionService fractionService;

    @Autowired
    private ProfileService profileService;

    @GetMapping
    public ResponseEntity<List<Fraction>> find() {
        List<Fraction> fractions = fractionService.find();
        if (CollectionUtils.isEmpty(fractions)) {
            return new ResponseEntity<>(NOT_FOUND);
        }
        fractions.sort(Comparator.comparing(Fraction::getMonth));
        return new ResponseEntity<>(fractions, OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody List<Fraction> fractions) {

        if (fractions.stream().anyMatch(f -> StringUtils.isBlank(f.getProfile()))) {
            return new ResponseEntity<>(
                    ConsumptionResponse.buildResponse(false, "Profile(s) for fraction is empty of null"), BAD_REQUEST);
        }

        ConsumptionResponse consumptionResponse =
                fractionService.create(fractions.stream().collect(Collectors.groupingBy(Fraction::getProfile)));

        return consumptionResponse.isSuccess() ? new ResponseEntity<>(consumptionResponse, CREATED) :
                new ResponseEntity<>(consumptionResponse, BAD_REQUEST);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody List<Fraction> fractions) {
        ConsumptionResponse consumptionResponse =
                fractionService.update(fractions.stream().collect(Collectors.groupingBy(Fraction::getProfile)));

        return consumptionResponse.isSuccess() ? new ResponseEntity<>(consumptionResponse, OK) :
                new ResponseEntity<>(consumptionResponse, BAD_REQUEST);
    }

    @DeleteMapping("/profile/{profile}")
    public ResponseEntity<?> delete(@PathVariable String profile) {
        ConsumptionResponse consumptionResponse = profileService.delete(profile);
        return consumptionResponse.isSuccess() ? new ResponseEntity<>(consumptionResponse, OK) :
                new ResponseEntity<>(consumptionResponse, BAD_REQUEST);
    }

}
