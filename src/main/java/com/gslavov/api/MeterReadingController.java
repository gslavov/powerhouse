package com.gslavov.api;

import com.gslavov.api.model.Consumption;
import com.gslavov.api.model.MeterReading;
import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.model.Month;
import com.gslavov.service.MeterReadingService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/api/meter/reading", produces = APPLICATION_JSON_VALUE)
public class MeterReadingController {

    @Autowired
    private MeterReadingService service;

    @GetMapping
    public ResponseEntity<?> find() {
        List<MeterReading> meterReadings = service.find();

        if (CollectionUtils.isEmpty(meterReadings)) {
            return new ResponseEntity<>(NOT_FOUND);
        }
        meterReadings.sort(Comparator.comparing(MeterReading::getMonth));
        return new ResponseEntity<>(meterReadings, OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@RequestBody List<MeterReading> meterReadings) {
        if (meterReadings.stream().anyMatch(m -> StringUtils.isBlank(m.getProfile()))) {
            return new ResponseEntity<>(
                    ConsumptionResponse.buildResponse(false, "Profile(s) for meter reading is empty of null"),
                    BAD_REQUEST);
        }

        ConsumptionResponse consumptionResponse =
                service.create(meterReadings.stream().collect(Collectors.groupingBy(MeterReading::getProfile)));

        return consumptionResponse.isSuccess() ? new ResponseEntity<>(consumptionResponse, CREATED) :
                new ResponseEntity<>(consumptionResponse, BAD_REQUEST);
    }

    @PutMapping
    public ResponseEntity<?> update(@RequestBody List<MeterReading> meterReadings) {
        ConsumptionResponse consumptionResponse =
                service.update(meterReadings.stream().collect(Collectors.groupingBy(MeterReading::getProfile)));

        return consumptionResponse.isSuccess() ? new ResponseEntity<>(consumptionResponse, OK) :
                new ResponseEntity<>(consumptionResponse, BAD_REQUEST);
    }

    @DeleteMapping("/{deviceId}")
    public ResponseEntity<?> delete(@PathVariable long deviceId) {
        ConsumptionResponse consumptionResponse = service.delete(deviceId);
        return consumptionResponse.isSuccess() ? new ResponseEntity<>(consumptionResponse, OK) :
                new ResponseEntity<>(consumptionResponse, BAD_REQUEST);
    }

    @GetMapping("/device/{deviceId}/{month}")
    public ResponseEntity<?> getConsumption(@PathVariable long deviceId, @PathVariable Month month) {
        Integer monthConsumption = service.getMonthConsumption(deviceId, month);
        if (monthConsumption == null) {
            return new ResponseEntity<>(NOT_FOUND);
        }
        return new ResponseEntity<>(Consumption.builder().value(monthConsumption).build(), OK);
    }
}
