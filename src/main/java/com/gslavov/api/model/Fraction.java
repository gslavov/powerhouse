package com.gslavov.api.model;

import com.gslavov.repository.model.Month;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Fraction {

    private Month month;
    private String profile;
    private float fraction;
}
