package com.gslavov.api.responce;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ConsumptionResponse {

    private boolean success;
    private String message;


    public static ConsumptionResponse buildResponse(boolean success, String message) {
        return ConsumptionResponse.builder()
                                  .success(success)
                                  .message(message)
                                  .build();
    }

}
