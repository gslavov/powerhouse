package com.gslavov;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableAutoConfiguration
@SpringBootApplication
public class EnergyConsumptionApp {

    public static void main(String... args) {
        SpringApplication.run(EnergyConsumptionApp.class, args);
    }

}
