package com.gslavov.repository;

import com.gslavov.repository.model.MeterReadingEntity;
import com.gslavov.repository.model.Month;
import com.gslavov.repository.model.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MeterReadingRepository extends JpaRepository<MeterReadingEntity, Long> {

    MeterReadingEntity findByProfileAndMonth(ProfileEntity profile, Month month);

    List<MeterReadingEntity> findByDeviceId(long deviceId);

    MeterReadingEntity findByDeviceIdAndMonth(long deviceId, Month month);
}
