package com.gslavov.repository;

import com.gslavov.repository.model.FractionEntity;
import com.gslavov.repository.model.Month;
import com.gslavov.repository.model.ProfileEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FractionRepository extends JpaRepository<FractionEntity, Long> {

    FractionEntity findByProfileAndMonth(ProfileEntity profile, Month month);
}
