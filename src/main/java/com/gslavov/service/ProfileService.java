package com.gslavov.service;

import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.model.ProfileEntity;

public interface ProfileService {

    ProfileEntity findByName(String name);

    ProfileEntity save(ProfileEntity profileEntity);

    ConsumptionResponse delete(String name);

}
