package com.gslavov.service.impl;

import com.gslavov.api.model.Fraction;
import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.FractionRepository;
import com.gslavov.repository.model.FractionEntity;
import com.gslavov.repository.model.Month;
import com.gslavov.repository.model.ProfileEntity;
import com.gslavov.service.FractionService;
import com.gslavov.service.ProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.transaction.Transactional;

import static com.gslavov.api.responce.ConsumptionResponse.buildResponse;


@Slf4j
@Service
@Transactional
public class FractionServiceImpl implements FractionService {

    @Autowired
    private FractionRepository fractionRepository;

    @Autowired
    private ProfileService profileService;


    @Override
    public ConsumptionResponse create(Map<String, List<Fraction>> profileFractions) {
        Set<FractionEntity> fractions = new HashSet<>();
        if (!validateFraction(profileFractions)) {
            return buildResponse(false, "For a given Profile the sum of all fractions should be 1");
        }
        for (Map.Entry<String, List<Fraction>> entry : profileFractions.entrySet()) {
            if (profileService.findByName(entry.getKey()) != null) {
                log.info("Profile '{}' already exist", entry.getKey());
                return buildResponse(false, "Profile '" + entry.getKey() + "' already exist");
            }
            if (entry.getValue().size() < 12) {
                return buildResponse(false, "Not all months are  specified");
            }
            ProfileEntity profile = createProfile(entry.getKey());
            for (Fraction fraction : entry.getValue()) {
                FractionEntity fractionEntity = createFraction(fraction, profile);
                fractions.add(fractionEntity);
            }
            profile.setFractions(fractions);
            profileService.save(profile);
        }
        return buildResponse(true, "Profile(s) and Fractions has been successfully created");
    }

    @Override
    public ConsumptionResponse update(Map<String, List<Fraction>> profileFractions) {
        for (Map.Entry<String, List<Fraction>> entry : profileFractions.entrySet()) {
            ProfileEntity profile = profileService.findByName(entry.getKey());
            if (profile == null) {
                log.info("Profile '{}' does not exist", entry.getKey());
                return buildResponse(false, "Profile '" + entry.getKey() + "' does not exist");
            }
            if (entry.getValue().size() < 12) {
                return buildResponse(false, "Not all months are specified");
            }
            if (!validateFraction(profileFractions)) {
                return buildResponse(false, "For a given Profile the sum of all fractions should be 1");
            }
            for (Fraction fraction : entry.getValue()) {
                FractionEntity fractionEntity =
                        fractionRepository.findByProfileAndMonth(profile, fraction.getMonth());
                fractionEntity.setValue(fraction.getFraction());
                save(fractionEntity);
            }
        }
        return buildResponse(true, "Fractions has been successfully updated");
    }

    @Override
    public FractionEntity save(FractionEntity fractionEntity) {
        return fractionRepository.save(fractionEntity);
    }

    @Override
    public List<Fraction> find() {
        return buildFractions(findAll());
    }

    @Override
    public List<FractionEntity> findAll() {
        return fractionRepository.findAll();
    }

    @Override
    public FractionEntity findByProfileAndMonth(ProfileEntity profile, Month month) {
        return fractionRepository.findByProfileAndMonth(profile, month);
    }

    @Override
    public boolean validateFraction(Map<String, List<Fraction>> profileFractions) {
        for (Map.Entry<String, List<Fraction>> entry : profileFractions.entrySet()) {
            BigDecimal sum = new BigDecimal(0.0);
            for (Fraction fraction : entry.getValue()) {
                sum = sum.add(new BigDecimal(fraction.getFraction()));
            }
            sum = sum.setScale(2, RoundingMode.FLOOR);
            if (!sum.equals(new BigDecimal(1.00).setScale(2, RoundingMode.FLOOR))) {
                return false;
            }
        }
        return true;
    }

    private FractionEntity createFraction(Fraction fraction, ProfileEntity profile) {
        return FractionEntity.builder()
                             .profile(profile)
                             .month(fraction.getMonth())
                             .value(fraction.getFraction())
                             .build();

    }

    private ProfileEntity createProfile(String name) {
        return ProfileEntity.builder().name(name).build();
    }

    private List<Fraction> buildFractions(List<FractionEntity> fractionEntities) {
        List<Fraction> fractions = new ArrayList<>();
        for (FractionEntity fractionEntity : fractionEntities) {
            fractions.add(Fraction.builder()
                                  .profile(fractionEntity.getProfile().getName())
                                  .month(fractionEntity.getMonth())
                                  .fraction(fractionEntity.getValue())
                                  .build());
        }
        return fractions;
    }

}
