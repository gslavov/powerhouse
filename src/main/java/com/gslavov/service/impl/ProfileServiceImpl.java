package com.gslavov.service.impl;

import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.ProfileRepository;
import com.gslavov.repository.model.ProfileEntity;
import com.gslavov.service.ProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import static com.gslavov.api.responce.ConsumptionResponse.buildResponse;

@Slf4j
@Service
@Transactional
public class ProfileServiceImpl implements ProfileService {

    @Autowired
    private ProfileRepository repository;

    @Override
    public ProfileEntity findByName(String name) {
        return repository.findByName(name);
    }

    @Override
    public ProfileEntity save(ProfileEntity profileEntity) {
        return repository.save(profileEntity);
    }

    @Override
    public ConsumptionResponse delete(String name) {
        ProfileEntity profile = repository.findByName(name);
        if (profile == null) {
            log.info("Profile '{}' does not exist", name);
            return buildResponse(false, "Profile '" + name + "' does not exist");
        }
        repository.delete(profile.getId());
        return buildResponse(true, "Profile '" + name + "' and fractions has been deleted");
    }
}
