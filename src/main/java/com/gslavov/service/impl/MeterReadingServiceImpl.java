package com.gslavov.service.impl;

import com.gslavov.api.model.MeterReading;
import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.MeterReadingRepository;
import com.gslavov.repository.model.MeterReadingEntity;
import com.gslavov.repository.model.Month;
import com.gslavov.repository.model.ProfileEntity;
import com.gslavov.service.FractionService;
import com.gslavov.service.MeterReadingService;
import com.gslavov.service.ProfileService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import static com.gslavov.repository.model.Month.DEC;
import static com.gslavov.repository.model.Month.JAN;
import static com.gslavov.api.responce.ConsumptionResponse.buildResponse;

@Slf4j
@Service
@Transactional
public class MeterReadingServiceImpl implements MeterReadingService {

    //TODO: put to config file
    private static final double TOLERANCE = 0.25;

    @Autowired
    private ProfileService profileService;

    @Autowired
    private FractionService fractionService;

    @Autowired
    private MeterReadingRepository repository;

    @Override
    public ConsumptionResponse create(Map<String, List<MeterReading>> metersReading) {
        for (Map.Entry<String, List<MeterReading>> entry : metersReading.entrySet()) {
            ProfileEntity profile = findProfile(entry.getKey());
            if (profile == null) {
                return buildResponse(false, "Profile '" + entry.getKey() + "' doesn't exist");
            }
            if (entry.getValue().size() < 12) {
                return buildResponse(false, "Not all months are specified");
            }
            if (!validateMonthsValue(entry.getValue())) {
                return buildResponse(false,
                        "For a given meter reading month should not be lower than the previous one");
            }
            if (!validateConsumptionForMonth(entry.getValue(), profile)) {
                return buildResponse(false, "Not allowed values for Consumption");
            }
            createMeterReading(entry.getValue(), profile);
        }
        return buildResponse(true, "Meter Readings has been successfully submitted");
    }

    @Override
    public List<MeterReading> find() {
        return buildMeterReading(findAll());
    }

    @Override
    public List<MeterReadingEntity> findAll() {
        return repository.findAll();
    }

    @Override
    public ConsumptionResponse update(Map<String, List<MeterReading>> metersReading) {
        for (Map.Entry<String, List<MeterReading>> entry : metersReading.entrySet()) {
            ProfileEntity profile = findProfile(entry.getKey());
            if (profile == null) {
                log.info("Profile '{}' does not exist", entry.getKey());
                return buildResponse(false, "Profile '" + entry.getKey() + "' does not exist");
            }
            if (entry.getValue().size() < 12) {
                return buildResponse(false, "Not all months are specified");
            }
            if (!validateMonthsValue(entry.getValue())) {
                return buildResponse(false,
                        "For a given meter reading month should not be lower than the previous one");
            }
            if (!validateConsumptionForMonth(entry.getValue(), profile)) {
                return buildResponse(false, "Not allowed values for Consumption");
            }
            for (MeterReading meterReading : entry.getValue()) {
                MeterReadingEntity meterReadingEntity =
                        repository.findByProfileAndMonth(profile, meterReading.getMonth());
                meterReadingEntity.setValue(meterReading.getValue());
                save(meterReadingEntity);
            }
        }
        return buildResponse(true, "Meter Readings has been updated");
    }

    @Override
    public MeterReadingEntity save(MeterReadingEntity meterReadingEntity) {
        return repository.save(meterReadingEntity);
    }

    @Override
    public ConsumptionResponse delete(long deviceId) {
        List<MeterReadingEntity> meterReadingEntities = repository.findByDeviceId(deviceId);
        if (CollectionUtils.isEmpty(meterReadingEntities)) {
            return buildResponse(false, "Meter Readings for device " + deviceId + " has not been found");
        }
        repository.delete(meterReadingEntities);
        return buildResponse(true, "Meter Readings for device " + deviceId + " has been deleted");
    }

    @Override
    public Integer getMonthConsumption(long deviceId, Month month) {
        MeterReadingEntity meterReadingForCurrentMonth = repository.findByDeviceIdAndMonth(deviceId, month);
        if (meterReadingForCurrentMonth == null) {
            return null;
        }
        if (JAN.equals(month)) {
            return meterReadingForCurrentMonth.getValue();
        }
        int currentMonthOrdinal = month.ordinal();
        Month previousMonth = month.values()[currentMonthOrdinal - 1];
        MeterReadingEntity meterReadingForPreviousMonth = repository.findByDeviceIdAndMonth(deviceId, previousMonth);
        int consumption = meterReadingForCurrentMonth.getValue() - meterReadingForPreviousMonth.getValue();
        return consumption;
    }

    protected boolean validateConsumptionForMonth(List<MeterReading> meterReadings, ProfileEntity profile) {
        meterReadings.sort(Comparator.comparing(MeterReading::getMonth));
        int totalConsumption = getTotalConsumption(meterReadings);
        for (int i = 0; i < meterReadings.size() - 1; i++) {
            int consumption;
            MeterReading currentMeterReading = meterReadings.get(i);
            if (JAN.equals(currentMeterReading.getMonth())) {
                consumption = currentMeterReading.getValue();
            } else {
                MeterReading previousMeterReading = meterReadings.get(i - 1);
                consumption = currentMeterReading.getValue() - previousMeterReading.getValue();
            }
            float fractionValue =
                    fractionService.findByProfileAndMonth(profile, currentMeterReading.getMonth()).getValue();
            int current = (int) (totalConsumption * fractionValue);
            int bottomBound = (int) (current - current * TOLERANCE);
            int upperBound = (int) (current + current * TOLERANCE);
            if (!(bottomBound <= consumption && consumption <= upperBound)) {
                return false;
            }
        }
        return true;
    }

    protected boolean validateMonthsValue(List<MeterReading> meterReadings) {
        meterReadings.sort(Comparator.comparing(MeterReading::getMonth));
        for (int i = 0; i < meterReadings.size() - 1; i++) {
            if (meterReadings.get(i).getValue() > meterReadings.get(i + 1).getValue()) {
                return false;
            }
        }
        return true;
    }

    private void createMeterReading(List<MeterReading> meterReadings, ProfileEntity profile) {
        List<MeterReadingEntity> entities = new ArrayList<>();
        for (MeterReading meterReading : meterReadings) {
            entities.add(MeterReadingEntity.builder()
                                           .month(meterReading.getMonth())
                                           .value(meterReading.getValue())
                                           .profile(profile)
                                           .deviceId(meterReading.getDeviceId())
                                           .build());
        }
        repository.save(entities);
    }

    private List<MeterReading> buildMeterReading(List<MeterReadingEntity> meterReadingEntities) {
        List<MeterReading> meterReadings = new ArrayList<>();
        for (MeterReadingEntity entity : meterReadingEntities) {
            meterReadings.add(MeterReading.builder()
                                          .month(entity.getMonth())
                                          .profile(entity.getProfile().getName())
                                          .value(entity.getValue())
                                          .deviceId(entity.getDeviceId())
                                          .build());
        }
        return meterReadings;
    }

    private int getTotalConsumption(List<MeterReading> meterReadings) {
        return meterReadings.stream()
                            .filter(meterReading -> DEC.equals(meterReading.getMonth()))
                            .findFirst()
                            .map(MeterReading::getValue)
                            .orElse(0);
    }

    private ProfileEntity findProfile(String profile) {
        return profileService.findByName(profile);
    }

}
