package com.gslavov.service;

import com.gslavov.api.model.MeterReading;
import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.model.MeterReadingEntity;
import com.gslavov.repository.model.Month;

import java.util.List;
import java.util.Map;

public interface MeterReadingService {

    ConsumptionResponse create(Map<String, List<MeterReading>> metersReading);

    List<MeterReading> find();

    List<MeterReadingEntity> findAll();

    ConsumptionResponse update(Map<String, List<MeterReading>> metersReading);

    MeterReadingEntity save(MeterReadingEntity meterReadingEntity);

    ConsumptionResponse delete(long deviceId);

    Integer getMonthConsumption(long deviceId, Month month);

}
