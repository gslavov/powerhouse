package com.gslavov.service;

import com.gslavov.api.model.Fraction;
import com.gslavov.api.responce.ConsumptionResponse;
import com.gslavov.repository.model.FractionEntity;
import com.gslavov.repository.model.Month;
import com.gslavov.repository.model.ProfileEntity;

import java.util.List;
import java.util.Map;

public interface FractionService {

    ConsumptionResponse create(Map<String, List<Fraction>> profileFractions);

    ConsumptionResponse update(Map<String, List<Fraction>> profileFractions);

    FractionEntity save(FractionEntity fractionEntity);

    List<Fraction> find();

    List<FractionEntity> findAll();

    FractionEntity findByProfileAndMonth(ProfileEntity profile, Month month);

    boolean validateFraction(Map<String, List<Fraction>> profileFractions);
}
