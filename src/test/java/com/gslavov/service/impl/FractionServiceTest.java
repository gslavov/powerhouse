package com.gslavov.service.impl;

import com.gslavov.api.model.Fraction;
import com.gslavov.service.FractionService;
import com.gslavov.service.impl.FractionServiceImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FractionServiceTest {

    private FractionService fractionService = new FractionServiceImpl();


    @Test
    public void testValidFraction() throws Exception {
        Map<String, List<Fraction>> profileFractions = generateProfileFractions(true);
        Assert.assertTrue(fractionService.validateFraction(profileFractions));
    }

    @Test
    public void testInvalidFraction() throws Exception {
        Map<String, List<Fraction>> profileFractions = generateProfileFractions(false);
        Assert.assertFalse(fractionService.validateFraction(profileFractions));
    }

    private Map<String, List<Fraction>> generateProfileFractions(boolean isFractionValid) {
        Map<String, List<Fraction>> profileFractions = new HashMap<>();
        if (isFractionValid) {
            profileFractions.put("A", generateValidFractions());
        } else {
            profileFractions.put("B", generateInvalidFractions());
        }
        return profileFractions;
    }

    private List<Fraction> generateValidFractions() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(buildFraction(0.1F));
        fractions.add(buildFraction(0.2F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.15F));
        fractions.add(buildFraction(0.1F));
        fractions.add(buildFraction(0.1F));
        return fractions;
    }

    private List<Fraction> generateInvalidFractions() {
        List<Fraction> fractions = new ArrayList<>();
        fractions.add(buildFraction(0.1F));
        fractions.add(buildFraction(0.2F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.15F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.35F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.05F));
        fractions.add(buildFraction(0.15F));
        fractions.add(buildFraction(0.1F));
        fractions.add(buildFraction(0.1F));
        return fractions;
    }

    private Fraction buildFraction(Float value) {
        return Fraction.builder().fraction(value).build();
    }

}