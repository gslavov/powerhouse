package com.gslavov.service.impl;

import com.gslavov.api.model.MeterReading;
import com.gslavov.repository.model.FractionEntity;
import com.gslavov.repository.model.Month;
import com.gslavov.repository.model.ProfileEntity;
import com.gslavov.service.FractionService;
import com.gslavov.service.impl.MeterReadingServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static com.gslavov.repository.model.Month.APR;
import static com.gslavov.repository.model.Month.AUG;
import static com.gslavov.repository.model.Month.DEC;
import static com.gslavov.repository.model.Month.FEB;
import static com.gslavov.repository.model.Month.JAN;
import static com.gslavov.repository.model.Month.JUL;
import static com.gslavov.repository.model.Month.JUN;
import static com.gslavov.repository.model.Month.MAR;
import static com.gslavov.repository.model.Month.MAY;
import static com.gslavov.repository.model.Month.NOV;
import static com.gslavov.repository.model.Month.OCT;
import static com.gslavov.repository.model.Month.SEP;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MeterReadingServiceTest {

    @Mock
    private FractionService fractionService;

    @InjectMocks
    private MeterReadingServiceImpl meterReadingService;

    private ProfileEntity profileEntity;

    @Before
    public void setUp() {
        profileEntity = ProfileEntity.builder()
                                     .name("A")
                                     .build();

        when(fractionService.findByProfileAndMonth(any(ProfileEntity.class), any(Month.class)))
                .thenReturn(buildFractionEntity(0.1F)).thenReturn(buildFractionEntity(0.2F))
                .thenReturn(buildFractionEntity(0.05F)).thenReturn(buildFractionEntity(0.05F))
                .thenReturn(buildFractionEntity(0.05F)).thenReturn(buildFractionEntity(0.05F))
                .thenReturn(buildFractionEntity(0.05F)).thenReturn(buildFractionEntity(0.05F))
                .thenReturn(buildFractionEntity(0.05F)).thenReturn(buildFractionEntity(0.15F))
                .thenReturn(buildFractionEntity(0.1F)).thenReturn(buildFractionEntity(0.1F));
    }

    @Test
    public void testValidConsumptionForMonth() throws Exception {
        List<MeterReading> meterReadings = generateValidMeterReading();

        Assert.assertTrue(meterReadingService.validateConsumptionForMonth(meterReadings, profileEntity));
    }

    @Test
    public void testInvalidConsumptionForMonth() throws Exception {
        List<MeterReading> meterReadings = generateInvalidMeterReading();

        Assert.assertFalse(meterReadingService.validateConsumptionForMonth(meterReadings, profileEntity));
    }

    @Test
    public void testValidateMonthsValue() throws Exception {
        List<MeterReading> meterReadings = generateValidMeterReading();

        Assert.assertTrue(meterReadingService.validateMonthsValue(meterReadings));
    }

    @Test
    public void testInvalidMonthsValue() throws Exception {
        List<MeterReading> meterReadings = generateInvalidMeterReading();

        Assert.assertFalse(meterReadingService.validateMonthsValue(meterReadings));
    }

    private FractionEntity buildFractionEntity(Float value) {
        return FractionEntity.builder()
                             .value(value)
                             .build();
    }

    private List<MeterReading> generateInvalidMeterReading() {
        List<MeterReading> meterReadings = new ArrayList<>();
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(JAN)
                                      .profile("A")
                                      .value(9)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(FEB)
                                      .profile("A")
                                      .value(19)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(MAR)
                                      .profile("A")
                                      .value(29)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(APR)
                                      .profile("A")
                                      .value(39)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(MAY)
                                      .profile("A")
                                      .value(35)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(JUN)
                                      .profile("A")
                                      .value(40)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(JUL)
                                      .profile("A")
                                      .value(45)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(AUG)
                                      .profile("A")
                                      .value(50)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(SEP)
                                      .profile("A")
                                      .value(58)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(OCT)
                                      .profile("A")
                                      .value(65)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(NOV)
                                      .profile("A")
                                      .value(75)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(DEC)
                                      .profile("A")
                                      .value(240)
                                      .build());
        return meterReadings;
    }

    private List<MeterReading> generateValidMeterReading() {
        List<MeterReading> meterReadings = new ArrayList<>();
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(SEP)
                                      .profile("A")
                                      .value(55)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(FEB)
                                      .profile("A")
                                      .value(22)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(MAR)
                                      .profile("A")
                                      .value(27)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(APR)
                                      .profile("A")
                                      .value(32)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(MAY)
                                      .profile("A")
                                      .value(37)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(JAN)
                                      .profile("A")
                                      .value(10)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(JUN)
                                      .profile("A")
                                      .value(40)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(JUL)
                                      .profile("A")
                                      .value(45)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(DEC)
                                      .profile("A")
                                      .value(85)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(AUG)
                                      .profile("A")
                                      .value(50)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(OCT)
                                      .profile("A")
                                      .value(65)
                                      .build());
        meterReadings.add(MeterReading.builder()
                                      .deviceId(2286)
                                      .month(NOV)
                                      .profile("A")
                                      .value(75)
                                      .build());
        return meterReadings;
    }
}